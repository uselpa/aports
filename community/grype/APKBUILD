# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=grype
pkgver=0.81.0
pkgrel=0
pkgdesc="Vulnerability scanner for container images, filesystems, and SBOMs"
url="https://github.com/anchore/grype"
license="Apache-2.0"
arch="all !armhf !armv7 !x86" # FTBFS on 32-bit arches
arch="$arch !ppc64le" # ppc64le: build constraints exclude all Go files in /home/buildozer/aports/testing/grype/src/pkg/mod/modernc.org/libc@v1.14.12/uuid/uuid
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/anchore/grype/archive/v$pkgver/grype-$pkgver.tar.gz"
options="!check" # tests need docker

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "
		-X main.version=$pkgver
		" \
		-o bin/grype ./cmd/grype

	./bin/grype completion bash > $pkgname.bash
	./bin/grype completion fish > $pkgname.fish
	./bin/grype completion zsh > $pkgname.zsh
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/grype -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
112f425184c532d304af37455e8512d4b16cd1e5ce86a5a604840b2987f01cee22b61c515b71a3bc9f18b4767db40c71f597fe094b5d266df20048421e2f3a68  grype-0.81.0.tar.gz
"
