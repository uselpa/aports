# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=syft
pkgver=1.13.0
pkgrel=0
pkgdesc="Generate a Software Bill of Materials (SBOM) from container images and filesystems"
url="https://github.com/anchore/syft"
license="Apache-2.0"
arch="all !armhf !armv7 !x86 !ppc64le !riscv64" # FTBFS on 32-bit arches, riscv64, ppc64le
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/anchore/syft/archive/v$pkgver/syft-$pkgver.tar.gz"
options="!check" # tests need docker

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "
		-X main.version=$pkgver
		" \
		-o bin/syft ./cmd/syft

	bin/syft completion bash > $pkgname.bash
	bin/syft completion fish > $pkgname.fish
	bin/syft completion zsh > $pkgname.zsh
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/syft -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
6224d328a27b36361a1b17ff62d9eee563f14e833cba7c23231ba55cece0c3abc3345f115c6d85416899a69f1da6197bf22933aebdc98036cacea3625429e49f  syft-1.13.0.tar.gz
"
